<?php
define('DEBUG', FALSE);
define('DIR_WEB', '/');
define('DIR_BASE', __DIR__ . '/../');
define('DIR_SERVICE_TESTS', DIR_BASE . 'src/ServicesAtHost/ServiceTests/');

require_once DIR_BASE . 'vendor/autoload.php';

$app = new ServicesAtHost\App();
$app->run();
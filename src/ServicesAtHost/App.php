<?php

namespace ServicesAtHost;

use ReflectionClass;

use Silex;
use Silex\Provider\Twig;

use ServicesAtHost\ServiceTests\ServiceTest;
use ServicesAtHost\Controller\DefaultControllerProvider;
use ServicesAtHost\Controller\APIControllerProvider;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;

class App extends Silex\Application {
  public static $twig;

  public function __construct(array $values = array()) {
    parent::__construct($values);

    date_default_timezone_set('Etc/UTC');

    if (DEBUG) {
      $this['debug'] = TRUE;

      error_reporting(E_ALL);
      ini_set('display_errors', TRUE);
      ini_set('display_startup_errors', TRUE);
    }

    $this->register(new Silex\Provider\TwigServiceProvider(), array(
       'twig.path' => DIR_BASE . 'templates',
    ));

    $this->registerRoutes();

    $this->error(function (\Exception $e, Request $request, $code) {
      if (DEBUG) {
        return null;
      }

      switch ($code) {
        case 404:
          return $this['twig']->render('page.twig', [
             'JS' => [ DIR_WEB . 'js/global.js' ],
             'CSS' => [
                DIR_WEB . 'css/global.css',
                'https://fonts.googleapis.com/css?family=Quicksand:400,700|VT323',
             ],
             'content' => $this['twig']->render('404.twig'),
          ]);
          break;
        default:
          return 'We are sorry, but something went terribly wrong.';
      }
    });
  }

  public function registerRoutes() {
    $this->mount('/', new DefaultControllerProvider());
    $this->mount('/api/', new APIControllerProvider());
  }

  public static function getServiceTests() {
    foreach (scandir(DIR_SERVICE_TESTS) as $file) {
      if ($file == '.' || $file == '..') {
        continue;
      }

      include_once DIR_SERVICE_TESTS . $file;
    }

    $classes = get_declared_classes();
    $implementations = array();

    foreach($classes as $class) {
      $reflect = new ReflectionClass($class);
      if($reflect->implementsInterface('ServicesAtHost\ServiceTests\ServiceTest'))
        $implementations[] = $class;
    }

    return $implementations;
  }

  public static function getServices($hostname, $ip) {
    $results = [];

    if ($hostname === NULL) {
      return null;
    }

    $classes = self::getServiceTests();

    foreach ($classes as $class) {
      $result = call_user_func("$class::getServers", $hostname, $ip);

      if ($result == NULL) {
        continue;
      }

      $results[] = [
         'title' => call_user_func("$class::getTitle"),
         'instances' => $result,
      ];
    }

    return $results;
  }
}
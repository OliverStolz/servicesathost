<?php

namespace ServicesAtHost;

use ServicesAtHost\ServiceTests;

class API {
  public static $Registry = [
     'v1' => [
        'GetServices' => [
           'description' => 'Get servers/services located at IP or hostname',
           'params' => [
              'host' => 'IP or hostname',
           ]
        ]
     ]
  ];

  public static function handleAPICall($version, $api_call, &$response) {
    if ($api_call === NULL) {
      $response['success'] = FALSE;
      $response['error'] = 'You have to specify an API call. Example: /api/v1/GetServers';
      return;
    }

    if (!array_key_exists($version, self::$Registry)) {
      $response['success'] = FALSE;
      $response['error'] = 'You specified an invalid API version. Available API versions are listed at /api-docs/';
      return;
    }

    if (!array_key_exists($api_call, self::$Registry[$version])) {
      $response['success'] = FALSE;
      $response['error'] = 'You specified an invalid API call. Available API calls are listed at /api-docs/';
      return;
    }

    if (!method_exists(__CLASS__, "API_$api_call")) {
      $response['success'] = FALSE;
      $response['error'] = 'The API call you are trying to use, is not implemented yet. Available API calls are listed at /api-docs/';
      return;
    }

    call_user_func_array(__CLASS__ . "::API_$api_call", [ &$response ]);
  }

  public static function API_GetServices(&$response) {
    $hostname = isset($_GET['host']) ? $_GET['host'] : NULL;
    $ip = gethostbyname($hostname);

    if ($hostname === NULL) {
      $response['success'] = FALSE;
      $response['error'] = 'You have to specify a host. Example: /api/v1/GetServers/?host=google.com';
      return;
    }

    $response['success'] = TRUE;
    $response['data'] = [];

    $classes = App::getServiceTests();

    foreach ($classes as $class) {
      $result = call_user_func("$class::getServers", $hostname, $ip);

      if ($result == NULL) {
        continue;
      }

      $response['data'][call_user_func("$class::getShortName", $hostname, $ip)] = $result;
    }
  }
}
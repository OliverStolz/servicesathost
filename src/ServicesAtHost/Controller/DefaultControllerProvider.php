<?php

namespace ServicesAtHost\Controller;

use ServicesAtHost\App;

use Silex\Application;
use Silex\ControllerCollection;
use Silex\Api\ControllerProviderInterface;

class DefaultControllerProvider implements ControllerProviderInterface
{
  /**
   * Returns routes to connect to the given application.
   *
   * @param Application $app An Application instance
   *
   * @return ControllerCollection A ControllerCollection instance
   */
  public function connect(Application $app) {
    $ctrl = $app['controllers_factory'];

    $ctrl->get('/', function () use ($app) {
      return $app['twig']->render('page.twig', [
         'JS' => [ DIR_WEB . 'js/global.js' ],
         'CSS' => [
            DIR_WEB . 'css/global.css',
            'https://fonts.googleapis.com/css?family=Quicksand:400,700|VT323',
         ],
         'content' => $app['twig']->render('form.twig'),
      ]);
    });

    $ctrl->get('/services/', function () use ($app) {
      $hostname = isset($_GET['host']) ? $_GET['host'] : NULL;
      $ip = gethostbyname($hostname);

      if ($hostname === NULL) {
        return $app->redirect('/');
      }

      $results = App::getServices($hostname, $ip);

      return $app['twig']->render('page.twig', [
         'JS' => [ DIR_WEB . 'js/global.js' ],
         'CSS' => [
            DIR_WEB . 'css/global.css',
            'https://fonts.googleapis.com/css?family=Quicksand:400,700|VT323',
         ],
         'content' => [
            $app['twig']->render('form.twig'),
            $app['twig']->render('results.twig', [
               'results' => $results,
               'host' => $hostname,
               'ip' => $ip,
            ]),
         ]
      ]);
    });

    return $ctrl;
  }
}
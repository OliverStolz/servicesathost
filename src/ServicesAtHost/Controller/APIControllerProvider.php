<?php

namespace ServicesAtHost\Controller;

use ServicesAtHost\API;

use Silex\Application;
use Silex\Api\ControllerProviderInterface;
use Silex\ControllerCollection;

class APIControllerProvider implements ControllerProviderInterface
{
  /**
   * Returns routes to connect to the given application.
   *
   * @param Application $app An Application instance
   *
   * @return ControllerCollection A ControllerCollection instance
   */
  public function connect(Application $app) {
    $api = $app['controllers_factory'];

    $api->get('/', function () use ($app) { return $app->redirect('/api/docs'); });

    $api->get('/docs/', function () use ($app) {
      return $app['twig']->render('page.twig', [
         'JS' => [ DIR_WEB . 'js/global.js' ],
         'CSS' => [
            DIR_WEB . 'css/global.css',
            'https://fonts.googleapis.com/css?family=Quicksand:400,700|VT323',
         ],
         'content' => $app['twig']->render('api-docs.twig', [ 'registry' => API::$Registry ]),
      ]);
    });

    $api->get('/{version}/{call}/', function ($version, $call) {
      $response = [];
      API::handleAPICall($version, $call, $response);

      return json_encode($response);
    });

    return $api;
  }
}
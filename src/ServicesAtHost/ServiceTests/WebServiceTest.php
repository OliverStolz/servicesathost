<?php

namespace ServicesAtHost\ServiceTests;

class WebServiceTest implements ServiceTest {

	const TIMEOUT = 3;

	public static function getTitle() {
		return 'Web Server';
	}

	public static function getShortName() {
		return 'web';
	}

	public static function getServers($host, $IP) {
		$response = [];
		$response[0] = [];

		if (empty($host) || $host === NULL) {
			return null;
		}

		if (self::isAvailable($host)) {
			$file_headers = @get_headers('http://' . $host);
		}

		if (empty($file_headers) || $file_headers[0] == 'HTTP/1.1 404 Not Found') {
			return null;
		}

		foreach($file_headers as $value) {
			$arr = explode(': ', $value);

			if (count($arr) == 2) {
				$response[0][$arr[0]] = $arr[1];
			}
			else {
				$response[0][] = $value;
			}
		}

		return $response;
	}

	private static function isAvailable($host) {
		if ($socket =@ fsockopen($host, 80, $errno, $errstr, self::TIMEOUT))
		{
			fclose($socket);
			return true;
		}
		return false;
	}
}
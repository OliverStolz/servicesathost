<?php

namespace ServicesAtHost\ServiceTests;

class SteamServiceTest implements ServiceTest {

	public static function getTitle() {
		return 'Steam Server';
	}

	public static function getShortName() {
		return 'steam';
	}

	public static function getServers($host, $IP) {
		$response = [];

		if (empty($host) || $host === NULL) {
			return null;
		}

		$contents = file_get_contents("http://api.steampowered.com/ISteamApps/GetServersAtAddress/v1?addr=$IP");

		if (empty($contents)) {
			return null;
		}

		$data = json_decode($contents, JSON_OBJECT_AS_ARRAY);

		if ($data['response']['success'] !== true || count($data['response']['servers']) == 0) {
			return null;
		}

		foreach ($data['response']['servers'] as $server) {
			$arr = [];

			foreach ($server as $key => $value) {
				$arr[$key] = $value;
			}

			$response[] = $arr;
		}

		return $response;
	}
}
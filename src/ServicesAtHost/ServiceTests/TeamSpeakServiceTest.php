<?php

namespace ServicesAtHost\ServiceTests;
use ts3admin;

class TeamSpeakServiceTest implements ServiceTest {

  const TIMEOUT = 5;
  const QUERY_PORT = 10011;

  public static function getTitle() {
    return 'TeamSpeak Server';
  }

  public static function getShortName() {
    return 'teamspeak';
  }

  public static function getServers($host, $IP) {
    $response = [];

    if (empty($host) || $host === NULL) {
      return null;
    }

    $ts = new ts3admin($IP, self::QUERY_PORT);

    // Connect to server
    if ($ts->succeeded($ts->connect())) {
      $serverID = 1;

      // Go through all server instances
      while ($ts->succeeded($ts->selectServer($serverID, 'serverId'))) {

        // Get server instance information
        $whoAmI = $ts->whoAmI();

        // Was getting instance information successful?
        if ($ts->succeeded($whoAmI)) {
          // Add server info, starting with 'virtualserver_' to array
          foreach ($whoAmI['data'] as $key => $value) {
            if (strpos($key, 'virtualserver_') === 0) {
              $response[$serverID][str_replace('virtualserver_' , '', $key)] = $value;
            }
          }
        }

        // Get servers client list
        $clientList = $ts->clientList();

        // Was getting client list successful?
        if ($ts->succeeded($clientList)) {
          $response[$serverID]['client_count'] = count($clientList['data']) - 1;
        }

        if (count($response[$serverID]) === 0) {
            // Could not get more information
            $response[] = [ 'Server found, but couldn\'t get further information' ];
        }

        $serverID++;
      }
    }
    else {
      return null;
    }

    return $response;
  }

}
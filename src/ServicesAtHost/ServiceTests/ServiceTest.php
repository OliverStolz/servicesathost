<?php

namespace ServicesAtHost\ServiceTests;

interface ServiceTest {
  public static function getTitle();
  public static function getShortName();
  public static function getServers($host, $IP);
}